from django.shortcuts import render
from .forms import UserRegistration
from .models import User
from django.http import JsonResponse
# from django.views.decorators.csrf import csrf_exempt
# Create your views here.
def home(request):
    form = UserRegistration()
    emp=User.objects.all()
    return render(request,'enroll/home.html',{'form':form, 'employee':emp})

# @csrf_exempt
def save_data(request):
    if request.method == "POST":
        form = UserRegistration(request.POST)
        if form.is_valid():
            sid = request.POST.get('empid')
            name= request.POST['name']
            email= request.POST['email']
            password= request.POST['password']

            if(sid==''): 
             usr = User(name= name, email=email, password=password)
            else:
             usr = User(id =sid, name= name, email=email, password=password)

            usr.save()
            emp = User.objects.values()
            # print(emp)
            employee_data = list(emp)
            return JsonResponse({'status': 'Save', 'employee_data': employee_data})
        else:
            return JsonResponse({'status': 0})

def delete_data(request):
    if request.method == 'POST':
        id = request.POST.get('sid')
        emp = User.objects.get(pk=id)
        emp.delete()
        return JsonResponse({'status':1})
    else:
        return JsonResponse({'status':0})

def edit_data(request):
    if request.method == 'POST':
        id = request.POST.get('sid')
        #print(id)
        employee = User.objects.get(pk=id)
        employee.save()
        employee_data = {"id": employee.id, "name": employee.name, "email": employee.email, "password": employee.password}
        #print(employee_data)
        #employee_data = list(employee_data)
        return JsonResponse(employee_data)
